const NepaliDate = require('nepali-date');
const moment = require('moment');

function getBS(date) {
    return new NepaliDate(date).format('YYYY-MM-DD');
}

function getBStoAD(date) {
    return new NepaliDate(date).getEnglishDate();
    // return new NepaliDate(date).getEnglishDate();
} 

function getADtoBS(date) {
    return new NepaliDate(new Date(date)).format('YYYY-MM-DD');
}

function getBSToday() {
    const date = new Date();
    return new NepaliDate(date).format('YYYY-MM-DD');
}


function getBSOneMonth() {
    const in_month = new Date(moment().add(1, 'M').format('YYYY-MM-DD'));
    return new NepaliDate(in_month).format('YYYY-MM-DD');
}

function getADToday() {
    return new Date(moment().format('YYYY-MM-DD'));
}

function getADOneMonth() {
    return new Date(moment().add(1, 'M').format('YYYY-MM-DD'));
}
function getAD(date) {
    return moment(new Date(date)).format('YYYY-MM-DD')
}

function checkBSDate(date) {
    try {
        new NepaliDate(date);
        return true;
    } catch (e) {
        return false;
    }
}

function checkADDate(date) {
    try {
        new NepaliDate(date);
        return true;
    } catch (e) {
        return false;
    }
}

function checkADDate(date) {
    const val = new Date(date);
    return val instanceof Date && !isNaN(val);
}

export { 
    getBSToday, getBSOneMonth, getBS, getBStoAD, getADtoBS, checkBSDate,
    getADToday, getADOneMonth, getAD, checkADDate,
};