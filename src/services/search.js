import Fuse from 'fuse.js';

var options = {
    shouldSort: true,
    threshold: 0.1,
    tokenize: false,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 4,
    keys: [
        "_id",
        "details.person.name",
        "details.person.company",
        "details.bank.name",
        "details.person.telephone"
    ]
};

function searchResult(list, variable) {
    var fuse = new Fuse(list, options);
    //console.log(list, variable)
    return fuse.search(variable);
}

export default searchResult;