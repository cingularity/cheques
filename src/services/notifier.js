const notifier = require('node-notifier');
import db from '../stores/persistent/db';


const updateDB = async (_id, changes) => {
    const response = await db.cheques.update(
        {_id: _id.toString()},
        { $set: changes}
    );
    return response;
}

async function notify(date, number) {
    notifier.notify({
        title: 'Cheque '.concat(number), 
        message: 'Due date is '.concat(date),
        wait: true,
    },
    function(err, response) {
        if (response === 'the user clicked on the toast.') {
            console.log('clicked')
            // remove the notification
            const changes = {
                'details.notified' : true,
            };
            updateDB(number, changes).then(value => console.log('notified', value));
        } else {
            console.log('Hide')
            // hide the notification for this session only
            return 'hid';
        }          
    });
}

notifier.on('click', function(notifierObject, options) {
    // Triggers if `wait: true` and user clicks notification
});
  
notifier.on('timeout', function(notifierObject, options) {
    // Triggers if `wait: true` and notification closes
});



export default notify;