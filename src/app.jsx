import React, { Fragment } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import red from '@material-ui/core/colors/red';
import { Provider } from 'mobx-react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Main from './app/main';
import store from './stores/local/mainStore';
//import 'typeface-roboto';



const theme = createMuiTheme({
    typography: {
        fontSize: 15,
        fontFamily: [
            'Roboto',
            '-apple-system',
            'BlinkMacSystemFont',
            'Segoe UI',
            'Arial',
            'sans-serif',
        ].join(','),
        useNextVariants: true,
    },
    palette: {
        primary: {
            light: '#FBC100',
            main: '#FBA100',
            dark: '#5f4339',
            contrastText: '#fff',
        },
        secondary: {
            light: '#e2f1f8',
            main: '#b0bec5',
            dark: '#808e95',
            contrastText: '#4e342e',
        },
        error: red,
    },
});


export default function App() {
    return (
        <Provider store={store}>
            <MuiThemeProvider theme={theme}>
                <Fragment>
                    <CssBaseline />
                    <Main />
                </Fragment>
            </MuiThemeProvider>
        </Provider>
    );
}
