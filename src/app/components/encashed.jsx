import React, { Component, Fragment } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Table from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Modal from '@material-ui/core/Modal';

import PayeeFormatter from './formatter/payeeFormatter';
import BankFormatter from './formatter/bankFormatter';
import DateFormatter from './formatter/dateFormatter';
import DeleteCheque from './helpers/deleteCheques';


function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}



const styles = theme => ({
    root: {
        //width: '100%',
        //maxHeight: 250,
        overflow: 'auto'
    },
    table: {
        minWidth: 700,
    },
    color: {
        backgroundColor: 'green'
    },
    button: {
        margin: theme.spacing.unit
    },
    note: {
        alignItems: 'center',
        marginLeft: theme.spacing.unit * 2
    },
    close: {
        backgroundColor: '#ffcdd2',
    },
    notClose: {
        backgroundColor: '#c8e6c9'
    },
    modalDelete: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '30%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
});

var date_sort_desc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return -1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return 1;
    return 0;
};

var date_sort_asc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return 1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return -1;
    return 0;
};


@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class ChequeEncashed extends Component {
    handleDelete = _id => event => {
        //store.chequeList.setCompleted(_id);
        console.log('deleting bank')
    };

    handleModalDeleteOpen = _id => event => {
        //this.setState({id: _id});
        const { store } = this.props;
        store.uiStore.modalDelete = true;
        store.chequeStore.CurrentID=_id;
        console.log('id clicked in encashed', _id)
        //console.log('in bounced', store.chequeStore.getCheque(_id))
        //this.setState({cheque: store.chequeStore.getCheque(_id)})
    };

    handleModalDeleteClose = event => {
        //this.setState({ id: null});
        const { store } = this.props;
        store.uiStore.modalDelete = false;
        store.chequeStore.CurrentID=-1;
        //this.setState({ cheque: ''})
    };
    
    
    render() {
        const { store, classes } = this.props;
        const cheques = store.chequeStore.encashed;
        return (
            <div>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Cheque Number</TableCell>
                                <TableCell>Dates</TableCell>
                                <TableCell>Details</TableCell>
                                <TableCell>Amount</TableCell>
                                <TableCell>Bank Name</TableCell>
                                <TableCell>Completed</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                cheques.slice()
                                .sort(date_sort_asc)
                                .map((result) => (
                                    <TableRow 
                                        key={result._id} 
                                        className={Math.ceil((new Date(result.details.cheque_encash_date) - new Date())/(1000*60*60*24)) < 3 ? classes.close : classes.notClose}>
                                        <TableCell align="left"><Typography variant="h6" gutterBottom>{result._id}</Typography></TableCell>
                                        {/* <TableCell component="th" scope="row">Recv: {result.details.cheque_received}, Due: {result.details.cheque_encash_date}</TableCell> */}
                                        <TableCell component="th" scope="row"><DateFormatter recv={result.details.cheque_received} due={result.details.cheque_encash_date}/></TableCell>
                                        <TableCell align="left"><PayeeFormatter person={result.details.person} /></TableCell>
                                        <TableCell align="left">{result.details.amount}</TableCell>
                                        <TableCell align="left"><BankFormatter bank={result.details.bank} /></TableCell>
                                        <TableCell>
                                            <Fragment>
                                                <Typography>Submited: {result.details.encashed_in}</Typography>
                                                <IconButton onClick={this.handleModalDeleteOpen(result._id)}><DeleteIcon /></IconButton>
                                            </Fragment>
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>
                </Paper>
                <Paper>
                    <DeleteModal id={store.chequeStore.CurrentID} />
                </Paper>
            </div>
        );
    }
}



@withStyles(styles, { withTheme: true })
@inject('store')
@observer
class DeleteModal extends Component {
    handleModalDeleteClose = event => {
        const { store } = this.props;
        store.uiStore.modalDelete = false;
    };
    
    render () {
        const { id, classes, store } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={store.uiStore.modalDelete}
                onClose={this.handleModalDeleteClose}
            >
                <div style={getModalStyle()} className={classes.modalDelete}>
                    <Typography variant="h5">Delete Cheque</Typography>
                        <DeleteCheque id={id} />
                </div>
            </Modal>
        );
    }
}