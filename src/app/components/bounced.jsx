import React, { Component, Fragment } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import classNames from 'classnames';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CommentIcon from '@material-ui/icons/Comment';
import DeleteIcon from '@material-ui/icons/Delete';
import Modal from '@material-ui/core/Modal';


import PayeeFormatter from './formatter/payeeFormatter';
import BankFormatter from './formatter/bankFormatter';
import DateFormatter from './formatter/dateFormatter';
import DeleteCheque from './helpers/deleteCheques';
import NoteUpdate from './helpers/noteUpdates';
import { Button } from '@material-ui/core';





const styles = theme => ({
    root: {
        //width: '100%',
        //maxHeight: 450,
        overflow: 'auto',
        flexGrow: 1,
    },
    margin: {
        margin: 0
    },
    padding: {
        padding: theme.spacing.unit,
    },
    table: {
        minWidth: 600,
    },
    tablecell: {
        fontSize: '10pt',
        width: '10%',
        padding: theme.spacing.unit,
    },
    color: {
        backgroundColor: 'green'
    },
    button: {
        margin: theme.spacing.unit
    },
    note: {
        alignItems: 'center',
        marginLeft: theme.spacing.unit * 2
    },
    close: {
        backgroundColor: '#ffcdd2',
    },
    notClose: {
        backgroundColor: '#c8e6c9'
    },
    modal: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '45%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    modalDelete: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '30%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '97%',
    },
});


function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

var date_sort_desc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return -1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return 1;
    return 0;
};

var date_sort_asc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return 1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return -1;
    return 0;
};



@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class ChequeBounced extends Component {
    handleModalCommentOpen = _id => event => {
        //this.setState({id: _id});
        const { store } = this.props;
        store.uiStore.modalComment = true;
        store.chequeStore.CurrentID=_id;
        //this.setState({cheque: store.chequeStore.getCheque(_id)})
    };
    handleModalCommentClose = event => {
        //this.setState({ id: null});
        const { store } = this.props;
        store.uiStore.modalComment = false;
        store.chequeStore.CurrentID=-1;
    };

    handleModalDeleteOpen = _id => event => {
        //this.setState({id: _id});
        const { store } = this.props;
        store.uiStore.modalDelete = true;
        store.chequeStore.CurrentID=_id;
        //console.log('in bounced', store.chequeStore.getCheque(_id))
        //this.setState({cheque: store.chequeStore.getCheque(_id)})
    };
    handleModalDeleteClose = event => {
        //this.setState({ id: null});
        const { store } = this.props;
        store.uiStore.modalDelete = false;
        store.chequeStore.CurrentID=-1;
        //this.setState({ cheque: ''})
    };
    handleMarkSent = _id => event => {
        event.preventDefault();
        const { store } = this.props;
        store.chequeStore.markAsSentToBank = _id;
    }
    
    
    render() {
        const { store, classes } = this.props;
        const cheques = store.chequeStore.bounced;
        return (
            <div>
                <Paper className={classNames(classes.root, classes.margin, classes.padding)}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Cheque Number</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Dates</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Details</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Amount</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Bank Name</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Comment</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                cheques.slice()
                                .sort(date_sort_asc)
                                .map((result) => (
                                    <TableRow 
                                        key={result._id} 
                                        className={Math.ceil((new Date(result.details.cheque_encash_date) - new Date())/(1000*60*60*24)) < 3 ? classes.close : classes.notClose}>
                                        <TableCell className={classes.tablecell} align="left" padding="dense"><Typography variant="button" gutterBottom>{result._id}</Typography></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense" ><DateFormatter recv={result.details.cheque_received} due={result.details.cheque_encash_date}/></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense"><PayeeFormatter person={result.details.person} /></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense">{result.details.amount}</TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense"><BankFormatter bank={result.details.bank} /></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense">
                                            { <Button onClick={this.handleMarkSent(result._id)} style={{fontSize: '12px'}}>Mark re-sent</Button>}
                                        </TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense">
                                            <Fragment>
                                                <IconButton onClick={this.handleModalCommentOpen(result._id)}><CommentIcon /></IconButton>
                                                <IconButton onClick={this.handleModalDeleteOpen(result._id)}><DeleteIcon /></IconButton>
                                            </Fragment>
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>
                </Paper>
                <Paper>
                    <CommentModal id={store.chequeStore.CurrentID} />
                </Paper>
                <Paper>
                    <DeleteModal id={store.chequeStore.CurrentID} />
                </Paper>
                {/* <Paper>
                    {store.uiStore.modalComment ? <CommentModal id={store.chequeStore.CurrentID} /> : null}
                </Paper>
                <Paper>
                    {store.uiStore.modalDelete ? <DeleteModal id={store.chequeStore.CurrentID} /> : null }
                </Paper> */}
            </div>
        );
    }
}


@withStyles(styles, { withTheme: true })
@inject('store')
@observer
class CommentModal extends Component {
    handleModalCommentClose = event => {
        const { store } = this.props;
        store.uiStore.modalComment = false;
    };
    
    render () {
        const { id, classes, store } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={store.uiStore.modalComment}
                onClose={this.handleModalCommentClose }
            >
                <div style={getModalStyle()} className={classes.modal}>
                    <Typography variant="h5">Update the cheque</Typography>
                        <NoteUpdate id={id} />
                </div>
            </Modal>
        );
    }
}

@withStyles(styles, { withTheme: true })
@inject('store')
@observer
class DeleteModal extends Component {
    handleModalDeleteClose = event => {
        const { store } = this.props;
        store.uiStore.modalDelete = false;
    };
    
    render () {
        const { id, classes, store } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={store.uiStore.modalDelete}
                onClose={this.handleModalDeleteClose}
            >
                <div style={getModalStyle()} className={classes.modalDelete}>
                    <Typography variant="h5">Delete Cheque</Typography>
                        <DeleteCheque id={id} />
                </div>
            </Modal>
        );
    }
}



// @withStyles(styles, { withTheme: true })
// @inject('store')
// @observer
// class DeleteCheque extends Component {
//     state = {
//         secondary: true,
//     };
//     handleNoButtonPress = event => {
//         const {uiStore} = this.props.store;
//         uiStore.modalDelete=false;
//     };
//     handleYesButtonPress = _id => event => {
//         const { store } = this.props;
//         console.log('will delete', _id)
//         store.chequeStore.deleteOneCheque(_id)
//         store.uiStore.modalDelete=false;
//     }
//     render() {
//         const { cheque, classes } = this.props;
//         return (
//             <Paper className={classNames(classes.paper, classes.margin)}>
//                 <Typography variant="h6">Are you sure you want to delete this cheque</Typography>
//                     <Paper>
//                         <Typography variant="button">{ cheque._id }</Typography>
//                         <Typography variant="body1">{ cheque.details.person.name }</Typography>
//                         <Typography variant="body1">{ cheque.details.person.company }</Typography>
//                     </Paper>
//                 <Button onClick={this.handleYesButtonPress(cheque._id)}>Yes</Button>
//                 <Button onClick={this.handleNoButtonPress}>No</Button>
//             </Paper>
//         );
//     } 
// }



// @withStyles(styles, { withTheme: true })
// @inject('store')
// @observer
// class NoteUpdate extends Component {
//     state = {
//         edit: false,
//         note: '',
//         id: ''
//     };
//     handleChange = name => event => {
//         this.setState({
//             [name] : event.target.value
//         });
//     };

//     handleSubmit = event => {
//         event.preventDefault();
//         const { store } = this.props;
//         store.chequeStore.updateComment(this.state.id, this.state.note);
//         store.uiStore.modalComment = false;
//         this.setState({ edit: false});
//     }

//     handleEdit = (id, current_note) => event => {
//         this.setState({ 
//             edit: true, 
//             id: id,
//             note: current_note
//         })
//     };
//     render() {
//         const { cheque, classes } = this.props
//         if (this.state.edit) {
//             return (
//                 <Paper className={classNames(classes.paper, classes.margin)}>
//                     <form onSubmit={this.handleSubmit}>
//                         <Typography>Editing cheque number {cheque._id}</Typography>
//                         <TextField 
//                             id="outlined-multiline-flexible"
//                             label="Update note"
//                             multiline
//                             rowsMax="4"
//                             value={this.state.note}
//                             onChange={this.handleChange('note')}
//                             className={classes.textField}
//                             margin="normal"
//                             helperText="Update the note for this bounced cheque."
//                             variant="outlined"
//                         />
//                         <Button 
//                             type="submit"
//                         >
//                             Update
//                         </Button>
//                     </form>
//                 </Paper>
//             );
//         } else {
//             return (
//                 <Paper className={classNames(classes.paper, classes.margin)}>
//                     <Typography variant="h6">Cheque No: {cheque._id}</Typography>
//                     <Typography variant="subtitle1" className={classes.note}>{cheque.details.note}</Typography>
                    
//                     <Button onClick={this.handleEdit(cheque._id, cheque.details.note)}>Edit</Button>
//                 </Paper>
//             );
//         }
//     }
// }