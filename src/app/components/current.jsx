import React, { Component, Fragment } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import classNames from 'classnames';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';

import PayeeFormatter from './formatter/payeeFormatter';
import BankFormatter from './formatter/bankFormatter';
import DateFormatter from './formatter/dateFormatter';
import DeleteCheque from './helpers/deleteCheques';
import EditCurrentCheque from './helpers/editCurrentCheques';





const styles = theme => ({
    root: {
        //width: '100%',
        //maxHeight: 450,
        overflow: 'auto',
        flexGrow: 1
    },
    margin: {
        margin: 0
    },
    padding: {
        padding: theme.spacing.unit,
    },
    table: {
        minWidth: 600,
    },
    tablecell: {
        fontSize: '10pt',
        width: '10%',
        padding: theme.spacing.unit,
    },
    color: {
        backgroundColor: 'green'
    },
    button: {
        margin: theme.spacing.unit
    },
    note: {
        alignItems: 'center',
        marginLeft: theme.spacing.unit * 2
    },
    close: {
        backgroundColor: '#ffcdd2',
    },
    notClose: {
        backgroundColor: '#c8e6c9'
    },
    modalDelete: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '30%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    modalEdit: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '30%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '97%',
    },
});

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}


var date_sort_desc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return -1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return 1;
    return 0;
};

var date_sort_asc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return 1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return -1;
    return 0;
};



@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class ChequeCurrent extends Component {

    handleSendToBank = _id => event => {
        const { store } = this.props;
        store.chequeStore.markAsSentToBank = _id;
    };

    handleModalDeleteOpen = _id => event => {
        const { store } = this.props;
        store.uiStore.modalDelete = true;
        console.log('id clicked in current', _id)
        store.chequeStore.CurrentID=_id;
    };

    handleModalDeleteClose = event => {
        this.setState({ id: null});
        const { store } = this.props;
        store.uiStore.modalDelete = false;
        store.chequeStore.CurrentID=-1;
    };
    
    handleModalEditOpen = _id => event => {
        const { store } = this.props;
        store.uiStore.modalEdit = true;
        console.log('id clicked in current', _id)
        store.chequeStore.CurrentID=_id;
    };

    handleModalEditClose = event => {
        this.setState({ id: null});
        const { store } = this.props;
        store.uiStore.modalEdit = false;
        store.chequeStore.CurrentID=-1;
    };
    
    render() {
        const { store, classes } = this.props;
        const cheques = store.chequeStore.current;
        return (
            <div>
                <Paper className={classNames(classes.root, classes.margin, classes.padding)}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Cheque Number</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Dates</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Details</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Amount</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Bank Name</TableCell>
                                <TableCell className={classes.tablecell} align="left" padding="dense">Next Step</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                cheques.slice()
                                .sort(date_sort_asc)
                                .map((result) => (
                                    <TableRow 
                                        key={result._id} 
                                        className={Math.ceil((new Date(result.details.cheque_encash_date) - new Date())/(1000*60*60*24)) < 3 ? classes.close : classes.notClose}>
                                        <TableCell className={classes.tablecell} align="left" padding="dense"><Typography variant="button" gutterBottom>{result._id}</Typography></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense"><DateFormatter recv={result.details.cheque_received} due={result.details.cheque_encash_date}/></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense"><PayeeFormatter person={result.details.person} /></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense">{result.details.amount}</TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense"><BankFormatter bank={result.details.bank} /></TableCell>
                                        <TableCell className={classes.tablecell} align="left" padding="dense">
                                            { <Button onClick={this.handleSendToBank(result._id)} style={{fontSize: '12px'}}>Mark Sent</Button>}
                                        </TableCell>
                                        <TableCell className={classes.tablecell}>
                                            <Fragment>
                                                <IconButton onClick={this.handleModalEditOpen(result._id)}><EditIcon /></IconButton>
                                                <IconButton onClick={this.handleModalDeleteOpen(result._id)}><DeleteIcon /></IconButton>
                                            </Fragment>
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>
                </Paper>
                <Paper>
                    <EditModal id={store.chequeStore.CurrentID} />
                    <DeleteModal id={store.chequeStore.CurrentID} />
                </Paper>
            </div>
        );
    }
}


@withStyles(styles, { withTheme: true })
@inject('store')
@observer
class EditModal extends Component {    
    handleModalEditClose = event => {
        const { store } = this.props;
        store.uiStore.modalEdit = false;
    };
    
    render () {
        const { id, classes, store } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={store.uiStore.modalEdit}
                onClose={this.handleModalEditClose}
            >
                <div style={getModalStyle()} className={classes.modalEdit}>
                    <Typography variant="h5">Edit Cheque</Typography>
                    <EditCurrentCheque id={id} />
                </div>
            </Modal>
        );
    }
}



@withStyles(styles, { withTheme: true })
@inject('store')
@observer
class DeleteModal extends Component {
    handleModalDeleteClose = event => {
        const { store } = this.props;
        store.uiStore.modalDelete = false;
    };
    
    render () {
        const { id, classes, store } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={store.uiStore.modalDelete}
                onClose={this.handleModalDeleteClose}
            >
                <div style={getModalStyle()} className={classes.modalDelete}>
                    <Typography variant="h5">Delete Cheque</Typography>
                    <DeleteCheque id={id} />
                </div>
            </Modal>
        );
    }
}
