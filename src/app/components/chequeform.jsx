import React, { Component } from 'react';
import classNames from 'classnames';
import { observer, inject } from "mobx-react";
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import { 
    getBSToday, getADToday, getBSOneMonth, 
    getADOneMonth, checkBSDate, checkADDate 
} from '../../services/nep_date';
import mapKeys from 'lodash/mapKeys';
import { Validation, fieldValidatorCore } from 'react-validation-framework';
import validator from 'validator';


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        maxHeight: 580,
        padding: theme.spacing.unit
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        flexBasis: 260,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 260,
        margin: 'dense'
    },
    button: {
        width: '60%',
        margin: theme.spacing.unit * 3,
    },
    grid: {
        //padding: theme.spacing.unit
    }
});


@withStyles(styles)
@inject('store')
@observer
export default class ChequeForm extends Component {
    constructor(props) {
        super(props);
        const { cheque, userStore } = this.props.store;
        const fields = [
            {"cheque_received": getADToday(), "calendar": 'AD', "cheque_encash_date":getADOneMonth()},
            {"cheque_received": getBSToday() , "calendar": 'BS', "cheque_encash_date": getBSOneMonth() }
        ];
        const relevant = fields.filter(cal => cal.calendar === userStore.calendar)[0];
        mapKeys(relevant, (value, key) => cheque.updateVariable(key, value));
    }

    handleChange = field => event => {
        const { cheque } = this.props.store;
        cheque.updateVariable(field, event.target.value);
        //console.log(field, event.target.value)
    };
    handleCalendarBlur = field => event => {
        const { cheque, userStore } = this.props.store;
        if (userStore.calendar='BS') {
            const valid = checkBSDate(event.target.value)
            !valid ? cheque.updateVariable(field, '') : cheque.updateVariable(field, event.target.value);
            return;
        } else {
            const valid = checkADDate(event.target.value)
            !valid ? cheque.updateVariable(field, '') : cheque.updateVariable(field, event.target.value);
            return;
        }
    }

    handleSubmit = event => {
        event.preventDefault();
        const { cheque, chequeStore, userStore, uiStore } = this.props.store;
        //  set the calendar and convert the date 
        cheque.setCalendar(userStore.calendar);
        chequeStore.addCheque(cheque.currentCheque);
        cheque.resetCheque();
        uiStore.modalAddCheque = false;
    }
    

    render() {
        const { classes, store } = this.props;
        const cheque = store.cheque;
        
        return (
            <Paper>
                <Grid container className={classNames(classes.root, classes.margin)} spacing={8}>
                    <form onSubmit={this.handleSubmit}>
                        <Grid item xs md className={classNames(classes.margin, classes.grid)}>
                            <TextField
                                className={classNames(classes.margin, classes.textField)}
                                label="Cheque Number"
                                value={cheque._id}
                                onChange={this.handleChange('_id')}
                                required
                            />
                        </Grid>
                        <Grid item xs={12} md className={classNames(classes.margin, classes.grid)}>
                            <TextField 
                                className={classNames(classes.margin, classes.textField)}
                                label="Received Date (YYYY-MM-DD)"
                                value={cheque.cheque_received}
                                onBlur={this.handleCalendarBlur('cheque_received')}
                                onChange={this.handleChange('cheque_received')}
                                InputProps={ 
                                    store.userStore.calendar === 'BS'
                                        ? {startAdornment: <InputAdornment position="start">BS</InputAdornment>,}
                                        : {startAdornment: <InputAdornment position="start">AD</InputAdornment>,}
                                } 
                                required 
                            />
                            <TextField 
                                className={classNames(classes.margin, classes.textField)}
                                label="Due Date (YYYY-MM-DD)"
                                //type='date'
                                // placeholder={
                                //     store.userStore.calendar === 'BS'
                                //         ? (cheque.cheque_encash_date === undefined ? bsInMonth : cheque.cheque_encash_date)
                                //         : (cheque.cheque_encash_date === undefined ? moment(new Date()).add(30, 'days').format('YYYY-MM-DD'): cheque.cheque_encash_date)
                                //     }
                                value={cheque.cheque_encash_date}
                                onBlur={this.handleCalendarBlur('cheque_encash_date')}
                                onChange={this.handleChange('cheque_encash_date')}
                                InputProps={ store.userStore.calendar === 'BS'
                                    ? {startAdornment: <InputAdornment position="start">BS</InputAdornment>,}
                                    : {startAdornment: <InputAdornment position="start">AD</InputAdornment>,}
                                } 
                                required
                            />
                        </Grid>
                        <Grid item xs={12} md className={classNames(classes.margin, classes.grid)}>
                            <InputLabel>Search Name</InputLabel>
                            <Select>
                            </Select>
                        </Grid>
                        <Grid item xs={12} md className={classNames(classes.margin, classes.grid)}>
                            <TextField
                                className={classNames(classes.margin, classes.textField)}
                                label="Amount"
                                value={cheque.amount}
                                onChange={this.handleChange('amount')}
                                required
                                InputProps={{startAdornment: <InputAdornment position="start">Rs</InputAdornment>,}}
                            />
                            <TextField
                                className={classNames(classes.margin, classes.textField)}
                                label="Note"
                                value={cheque.note}
                                onChange={this.handleChange('note')}
                                multiline
                            />
                        </Grid>
                        <Grid container spacing={0} justify="center" alignItems="flex-start" direction="column">
                            <Typography>Bank Details</Typography>
                            <Grid item xs md className={classNames(classes.margin, classes.grid)}>
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Bank"
                                    value={cheque.bank_name}
                                    onChange={this.handleChange('bank_name')}
                                    required
                                />
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Telephone"
                                    value={cheque.bank_telephone}
                                    onChange={this.handleChange('bank_telephone')}
                                />
                            </Grid>

                            <Typography>Person Details</Typography>
                            <Grid item xs md className={classNames(classes.margin, classes.grid)}>
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Name"
                                    value={cheque.person_name}
                                    onChange={this.handleChange('person_name')}
                                    required
                                />
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Company Name"
                                    value={cheque.person_company}
                                    onChange={this.handleChange('person_company')}
                                    required
                                />
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Telephone"
                                    value={cheque.person_telephone}
                                    onChange={this.handleChange('person_telephone')}
                                    //required
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            variant='outlined'
                            className={classes.button}
                        >
                            Add
                        </Button>
                    </form>
                </Grid>
            </Paper>
        );
    }
}