import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import ChequeForm from './chequeform';
import ChequeInBank from './sentToBank';
import ChequeBounced from './bounced';
import ChequeCurrent from './current';


import notify from '../../services/notifier';
import { getADtoBS } from "../../services/nep_date";



const styles = theme => ({
    root: {
        flexGrow: 1,
        maxHeight: '100vh',
        overflow: 'auto'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    paperground: {
        margin: theme.spacing.unit,
        //minHeight: '100vh',
        backgroundColor: '#28282A',
        elevation: 2,
        padding: theme.spacing.unit,
    },

    fab: {
        margin: theme.spacing.unit * 2,
        position: 'fixed',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2
    },
    paperCheque: {
        minHeight: '46vh',
        maxHeight: '46vh',
        overflow: 'auto',
        elevation: 2,
        marginTop: theme.spacing.unit,
    },
    card: {
        width: '100%',
        height: '100%',
        backgroundColor: '#FFF',
    },
    title: {
        fontSize: 22,
        padding: theme.spacing.unit * 2,
        marginTop: theme.spacing.unit,
        lineHeight: 0.2
    },
    modal: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '67%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
});


function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}



@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class Home extends Component {
    // state = {
    //     open: false,
    // };

    handleOpen = () => {
        //this.setState({ open: true });
        const {store} = this.props;
        store.uiStore.modalAddCheque = true;
    };
    
    handleClose = () => {
        //this.setState({ open: false });
        const {store} = this.props;
        store.uiStore.modalAddCheque = false;
    };


    async notifyCheque() {
        //console.log('called');
        const chequeData = this.props.store.chequeStore.current.slice();
        //console.log('logging notified', chequeData);
        chequeData.filter((cheque) => cheque.details.notified === false)
            .map((cheque) => {
                if (Math.ceil((new Date(cheque.details.cheque_encash_date) - new Date())/(5000 * 60 * 60 * 24)) < 2 ) {
                    const date = this.props.store.calendar !=='AD' 
                                    ? getADtoBS(cheque.details.cheque_encash_date)
                                    : new Date(cheque.details.cheque_encash_date)
                    notify(date, cheque._id);
                    this.props.store.chequeStore.hideNotification(cheque._id);
                }
            }
        );
        clearInterval(this.interval);
    }

    componentDidMount() {
        // const notification = async () => await this.notifyCheque();
        // this has to get the date and add it to mobx
        this.interval = setInterval(() => {
                this.notifyCheque();
            }, 
            5000
        );
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const { classes, store } = this.props;
        return (
            <div className={classes.root}>
                <Paper className={classes.paperground}>
                    <Grid container spacing={8}>
                        <Grid item xs={12} md={6}>
                            <Paper className={classes.paperCheque}>
                                <Card className={classes.card}>
                                    <Typography className={classes.title} gutterBottom color="primary">
                                        Received Cheques
                                    </Typography>
                                    <CardContent>
                                        <ChequeCurrent />
                                    </CardContent>
                                </Card>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <Paper className={classes.paperCheque}>
                                <Card className={classes.card}>
                                    <Typography className={classes.title} gutterBottom color='error'>
                                        Bounced Cheques
                                    </Typography>
                                    <CardContent>
                                        <ChequeBounced />
                                    </CardContent>
                                </Card>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Grid container >
                        <Grid item xs>
                            <Paper className={classes.paperCheque}>
                                <Card className={classes.card}>
                                    <Typography className={classes.title} gutterBottom>
                                        Cheques sent to bank
                                    </Typography>
                                    <CardContent>
                                        <ChequeInBank/>
                                    </CardContent>
                                </Card>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Fab 
                        variant="extended" 
                        color={'primary'} 
                        className={classes.fab} 
                        aria-label="Add a new cheque"
                        onClick={this.handleOpen}
                    >
                        <AddIcon />
                        Add New Cheque
                    </Fab>
                </Paper>
                <Paper> 
                    <Modal
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                        open={store.uiStore.modalAddCheque}
                        onClose={this.handleClose}
                    >
                        <div style={getModalStyle()} className={classes.modal}>
                            <Typography variant="h5">Add a new Cheque</Typography>
                            <ChequeForm />
                        </div>
                    </Modal>
                </Paper>          
            </div>
        );
    }
}

