import React, { Component, Fragment } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Table from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Modal from '@material-ui/core/Modal';

import PayeeFormatter from './formatter/payeeFormatter';
import BankFormatter from './formatter/bankFormatter';
import DateFormatter from './formatter/dateFormatter';
import DeleteCheque from './helpers/deleteCheques';


function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}



const styles = theme => ({
    root: {
        //width: '100%',
        //maxHeight: 250,
        overflow: 'auto'
    },
    table: {
        minWidth: 700,
    },
    color: {
        backgroundColor: 'green'
    },
    button: {
        margin: theme.spacing.unit
    },
    note: {
        alignItems: 'center',
        marginLeft: theme.spacing.unit * 2
    },
    close: {
        backgroundColor: '#ffcdd2',
    },
    notClose: {
        backgroundColor: '#c8e6c9'
    },
    modalDelete: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '30%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
});

var date_sort_desc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return -1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return 1;
    return 0;
};

var date_sort_asc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return 1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return -1;
    return 0;
};


@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class TotalCheques extends Component {
    handleDelete = _id => event => {
        //store.chequeList.setCompleted(_id);
        console.log('deleting bank')
    };

    handleModalDeleteOpen = _id => event => {
        //this.setState({id: _id});
        const { store } = this.props;
        store.uiStore.modalDelete = true;
        store.chequeStore.CurrentID=_id;
        console.log('id clicked in encashed', _id)
        //console.log('in bounced', store.chequeStore.getCheque(_id))
        //this.setState({cheque: store.chequeStore.getCheque(_id)})
    };

    handleModalDeleteClose = event => {
        //this.setState({ id: null});
        const { store } = this.props;
        store.uiStore.modalDelete = false;
        store.chequeStore.CurrentID=-1;
        //this.setState({ cheque: ''})
    };
    
    
    render() {
        const { store, classes } = this.props;
        const cheques = store.chequeStore.searched;
        return (
            <div>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell style={{width: '2rem'}} align="left">Cheque Number</TableCell>
                                <TableCell style={{width: '2rem'}} align="left">Dates</TableCell>
                                <TableCell style={{width: '2rem'}} align="left">Details</TableCell>
                                <TableCell style={{width: '2rem'}} align="left">Amount</TableCell>
                                <TableCell style={{width: '2rem'}} align="left">Bank Name</TableCell>
                                <TableCell style={{width: '2rem'}} align="left">Status</TableCell>
                                <TableCell style={{width: '25%'}} align="left">Note</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                cheques.slice()
                                //.sort(date_sort_asc)
                                .map((result) => (
                                    <TableRow 
                                        key={result._id} 
                                        // className={Math.ceil((new Date(result.details.cheque_encash_date) - new Date())/(1000*60*60*24)) < 3 ? classes.close : classes.notClose}
                                        >
                                        <TableCell style={{width: '4rem'}} align="left"><Typography variant="h6" gutterBottom>{result._id}</Typography></TableCell>
                                        {/* <TableCell component="th" scope="row">Recv: {result.details.cheque_received}, Due: {result.details.cheque_encash_date}</TableCell> */}
                                        <TableCell style={{width: '2rem'}} component="th" scope="row"><DateFormatter recv={result.details.cheque_received} due={result.details.cheque_encash_date}/></TableCell>
                                        <TableCell style={{width: '4rem'}} align="left"><PayeeFormatter person={result.details.person} /></TableCell>
                                        <TableCell style={{width: '2rem'}} align="left">{result.details.amount}</TableCell>
                                        <TableCell style={{width: '4rem'}} align="left"><BankFormatter bank={result.details.bank} /></TableCell>
                                        {/* <TableCell>
                                            { <IconButton onClick={this.handleModalDeleteOpen(result._id)}><DeleteIcon /></IconButton> }
                                        </TableCell> */}
                                        <TableCell style={{width: '2rem'}}>
                                            <Typography>{ result.details.sent_to_bank ? 'Sent to bank': null} </Typography>
                                            <Typography>{ result.details.bounced ? 'Bounced': null}</Typography>
                                            <Typography>{ 
                                                result.details.encashed 
                                                    ? result.details.encashed_in ? 'encashed: ' + result.details.encashed_in : 'Encashed: not Known'
                                                    : null}</Typography>
                                        </TableCell>
                                        <TableCell style={{width: '25%'}} align="left"><Typography>{result.details.note}</Typography></TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>
                </Paper>
                <Paper>
                    <DeleteModal id={store.chequeStore.CurrentID} />
                </Paper>
            </div>
        );
    }
}



@withStyles(styles, { withTheme: true })
@inject('store')
@observer
class DeleteModal extends Component {
    handleModalDeleteClose = event => {
        const { store } = this.props;
        store.uiStore.modalDelete = false;
    };
    
    render () {
        const { id, classes, store } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={store.uiStore.modalDelete}
                onClose={this.handleModalDeleteClose}
            >
                <div style={getModalStyle()} className={classes.modalDelete}>
                    <Typography variant="h5">Delete Cheque</Typography>
                        <DeleteCheque id={id} />
                </div>
            </Modal>
        );
    }
}