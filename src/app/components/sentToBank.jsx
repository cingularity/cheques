import React, { Component, Fragment } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import classNames from 'classnames';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import InputAdornment from '@material-ui/core/InputAdornment';

import PayeeFormatter from './formatter/payeeFormatter';
import BankFormatter from './formatter/bankFormatter';
import DateFormatter from './formatter/dateFormatter';
import { Paper, FormControl, FormControlLabel, TextField } from '@material-ui/core';



// const styles = theme => ({
//     root: {
//         width: '100%',
//         marginTop: theme.spacing.unit * 3,
//         maxHeight: 250,
//         overflow: 'auto'
//     },
//     table: {
//         minWidth: 700,
//     },
//     color: {
//         backgroundColor: 'green'
//     },
//     button: {
//         margin: theme.spacing.unit
//     },
//     note: {
//         alignItems: 'center',
//         marginLeft: theme.spacing.unit * 2
//     },
//     close: {
//         backgroundColor: '#ffcdd2',
//     },
//     notClose: {
//         backgroundColor: '#c8e6c9'
//     },
//     modalEncash: {
//         position: 'absolute',
//         width: theme.spacing.unit * 80,
//         //height: '30%',
//         backgroundColor: theme.palette.background.paper,
//         boxShadow: theme.shadows[5],
//         padding: theme.spacing.unit * 4,
//         outline: 'none',
//     },
// });


const styles = theme => ({
    root: {
        //width: '100%',
        //maxHeight: 450,
        overflow: 'auto',
        flexGrow: 1
    },
    margin: {
        margin: 0
    },
    padding: {
        padding: theme.spacing.unit,
    },
    table: {
        minWidth: 600,
    },
    tablecell: {
        fontSize: '10pt',
        width: '10%',
        padding: theme.spacing.unit,
    },
    color: {
        backgroundColor: 'green'
    },
    button: {
        margin: theme.spacing.unit
    },
    note: {
        alignItems: 'center',
        marginLeft: theme.spacing.unit * 2
    },
    close: {
        backgroundColor: '#ffcdd2',
    },
    notClose: {
        backgroundColor: '#c8e6c9'
    },
    modalEncash: {
        position: 'absolute',
        width: theme.spacing.unit * 80,
        //height: '30%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '97%',
    },
});

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}


var date_sort_desc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return -1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return 1;
    return 0;
};

var date_sort_asc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.
    if (date1.details.cheque_encash_date > date2.details.cheque_encash_date) return 1;
    if (date1.details.cheque_encash_date < date2.details.cheque_encash_date) return -1;
    return 0;
};



@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class ChequeInBank extends Component {
    handleEncashed = _id => event => {
        const { chequeStore } = this.props.store;
        chequeStore.markAsEncashed = _id;
    };

    handleBounced = _id => event => {
        const { chequeStore } = this.props.store;
        chequeStore.markAsBounced = _id;
    };

    handleBack = _id => event => {
        const { chequeStore } = this.props.store;
        chequeStore.markAsBack = _id;
    };

    handleModalEncashOpen = _id => event => {
        console.log('Opening the modal')
        const { store } = this.props;
        store.uiStore.modalEncash = true;
        store.chequeStore.CurrentID = _id
    }
    
    
    render() {
        const { store, classes } = this.props;
        const cheques = store.chequeStore.sentToBank;
        return (
            <div>
                <Paper className={classNames(classes.root, classes.margin, classes.padding)}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Cheque Number</TableCell>
                                <TableCell>Dates</TableCell>
                                <TableCell>Details</TableCell>
                                <TableCell>Amount</TableCell>
                                <TableCell>Bank Name</TableCell>
                                <TableCell>Next Step</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                cheques.slice()
                                .sort(date_sort_asc)
                                .map((result) => (
                                    <TableRow 
                                        key={result._id} 
                                        className={Math.ceil((new Date(result.details.cheque_encash_date) - new Date())/(1000*60*60*24)) < 3 ? classes.close : classes.notClose}>
                                        <TableCell align="left"><Typography variant="h6" gutterBottom>{result._id}</Typography></TableCell>
                                        {/* <TableCell component="th" scope="row">Recv: {result.details.cheque_received}, Due: {result.details.cheque_encash_date}</TableCell> */}
                                        <TableCell component="th" scope="row"><DateFormatter recv={result.details.cheque_received} due={result.details.cheque_encash_date}/></TableCell>
                                        <TableCell align="left"><PayeeFormatter person={result.details.person} /></TableCell>
                                        <TableCell align="left">{result.details.amount}</TableCell>
                                        <TableCell align="left"><BankFormatter bank={result.details.bank} /></TableCell>
                                        <TableCell>
                                            { <Button onClick={this.handleModalEncashOpen(result._id)} style={{fontSize: '12px', color: 'green'}}>mark encahsed</Button> }
                                            {/* { <Button onClick={this.handleEncashed(result._id)} style={{fontSize: '12px', color: 'green'}}>Mark Encashed</Button>} */}
                                            { <Button onClick={this.handleBounced(result._id)} style={{fontSize: '12px', color: 'red'}}>Mark Bounced</Button>}
                                            { <Button onClick={this.handleBack(result._id)} style={{fontSize: '12px'}}>Back</Button>}
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>
                </Paper>
                <Paper>
                    <EncashModal id={store.chequeStore.CurrentID}/>
                </Paper>
            </div>
        );
    }
}


@withStyles(styles, { withTheme: true })
@inject('store')
@observer
class EncashModal extends Component {
    handleModalEncashClose = event => {
        const {store} = this.props;
        store.uiStore.modalEncash = false;
    };

    handleEncashed = _id => event => {
        const { chequeStore } = this.props.store;
        chequeStore.onEncash = event.target.value;
        chequeStore.markAsEncashed = _id;
        this.handleModalEncashClose(event);
    };

    handleChange = event => {
        const { chequeStore } = this.props.store;
    }


    render () {
        const { id, classes , store } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={store.uiStore.modalEncash}
                onClose={this.handleModalEncashClose}
            >
                <div style={getModalStyle()} className={classes.modalEncash}>
                    <Typography variant="h5">Encash Cheque</Typography>
                    <FormControl component='fieldset' className={classes.formControl}>
                        <RadioGroup
                            name='Encashed in'
                            onChange={this.handleEncashed(id)}
                        >
                            <FormControlLabel value='bank' control={<Radio />} label='Bank a/c' />
                            <FormControlLabel value='cash' control={<Radio />} label='Cash' />
                        </RadioGroup>
                        <TextField 
                            label="Encashed on (YYYY-MM-DD)"
                            className={classes.textField}
                            onChange={this.handleChange}
                            margin="normal"
                            required
                            variant="filled"
                            InputProps={ store.userStore.calendar === 'BS'
                                    ? {startAdornment: <InputAdornment position="start">BS</InputAdornment>,}
                                    : {startAdornment: <InputAdornment position="start">AD</InputAdornment>,}
                                } 
                        />
                    </FormControl>
                </div>
            </Modal>
        );
    }
}