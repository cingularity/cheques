import React, { Component } from 'react';
import { observer, inject } from "mobx-react";
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const styles = theme => ({
    container: {
        //display: 'flex',
        //flexWrap: 'wrap',
        height: 70,
    },
    input: {
        margin: theme.spacing.unit,
        marginTop: 0
    },
    textField: {
        height: '60%',
        paddingLeft: theme.spacing.unit,
    }
});


@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class Search extends Component {
    handleSearch = event => {
        const { chequeStore } = this.props.store;
        chequeStore.Search = event.target.value;
        chequeStore.startSearch(event.target.value);
    }
    
    render() {
        const { store, classes } = this.props;
        const { chequeStore } = store;
        return (
            <div className={classNames(classes.container, classes.input)}>
                <Grid container>
                    <Grid item xs md>
                        <TextField
                            id="filled-name"
                            label="Search"
                            className={classes.textField}
                            value={chequeStore.Search}
                            onChange={this.handleSearch}
                            margin="normal"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item xs md>
                        <TextField
                            label="Date from"
                            className={classes.textField}
                            margin="normal"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item xs md>
                        <TextField
                            label="Date to"
                            className={classes.textField}
                            margin="normal"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }
}