import React, { Component } from "react"
import classNames from 'classnames';
import { observer } from "mobx-react";
import Paper from '@material-ui/core/Paper';
import TotalCheques from './totalCheques';
import Search from './search';
import { withStyles, Card } from "@material-ui/core";


const styles = theme => ({
    root: {
        //flexGrow: 1,
        maxHeight: '96vh',
        overflow: 'auto',
    },
    paperground: {
        margin: theme.spacing.unit,
        //minHeight: '100vh',
        backgroundColor: '#28282A',
        elevation: 2,
        padding: theme.spacing.unit,
    },
    card: {
        width: '100%',
        height: '92vh',
        backgroundColor: '#FFF',
        overflow: 'auto',
    },
    margin: {
        marginBottom: theme.spacing.unit * 1
    },
    padding: {
        paddingRight: theme.spacing.unit,
        paddingLeft: theme.spacing.unit
    },
    list: {
        //width: '100%',
        //maxHeight: 250,
        overflow: 'auto'
    },
});

@withStyles(styles)
@observer
export default class AllCheques extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Paper className={classNames(classes.margin, classes.padding, classes.paperground)}>
                    <Card className={classes.card}>
                        <Search />
                        <TotalCheques />
                    </Card>
                </Paper>
            </div>            
        );
    }
}