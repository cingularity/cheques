import React, { Component } from 'react';
import { withStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';

const styles = {
    root: {
        width: '100%',
        maxWidth: 100,
    },
    text: {
        fontSize: 14,
    }
};

@withStyles(styles)
class BankFormatter extends Component {
    render() {
        const { classes, bank } = this.props; // gets name, company, and telephone
        return (
            <div className={classes.root}>
                <Typography noWrap className={classes.text} variant="subtitle1" gutterBottom> { bank.name }</Typography>
                <Typography noWrap className={classes.text} variant="button" gutterBottom> { bank.telephone } </Typography>
            </div>
        );
    }
}


export default BankFormatter;