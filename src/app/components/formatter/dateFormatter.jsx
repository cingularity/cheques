import React, { Component } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';
import { getADtoBS } from '../../../services/nep_date';
import moment from 'moment'


const styles = {
    root: {
        width: '100%',
        maxWidth: 100,
    },
    text: {
        fontSize: 14,
        lineHeight: 1
    }
};

@inject('store')
@observer
@withStyles(styles)
class DateFormatter extends Component {
    render() {
        const { classes, recv, due, store } = this.props; // gets name and telephone
        
        return (
            <div className={classes.root}>
                {/* <p>{getADtoBS(recv)}</p> */}
                <Typography noWrap className={classes.text} variant="subtitle1" gutterBottom>Received On:</Typography>
                <Typography 
                    noWrap 
                    className={classes.text} 
                    variant="button" 
                    gutterBottom
                > 
                    { store.userStore.calendar !=='AD' ? getADtoBS(recv) : moment(recv).format('YYYY-MM-DD') } 
                </Typography>
                <Typography noWrap className={classes.text} variant="subtitle1" gutterBottom>Due On:</Typography>
                <Typography 
                    noWrap 
                    className={classes.text} 
                    variant="button" 
                    gutterBottom
                > 
                    { store.userStore.calendar !=='AD' ? getADtoBS(due) : moment(due).format('YYYY-MM-DD') } 
                </Typography>
            </div>
        );
    }
}

export default DateFormatter;