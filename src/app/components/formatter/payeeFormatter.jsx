import React, { Component } from 'react';
import { withStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';

const styles = {
    root: {
        width: '100%',
        maxWidth: 180,
    },
    text: {
        fontSize: 14,
    }
};

@withStyles(styles)
class PayeeFormatter extends Component {
    render() {
        const { classes, person } = this.props; // gets name, company, and telephone
        return (
            <div className={classes.root}>
                <Typography noWrap className={classes.text} variant="subtitle1" gutterBottom> { person.name }</Typography>
                <Typography noWrap className={classes.text} variant="subtitle2" gutterBottom> { person.company } </Typography>
                <Typography noWrap className={classes.text} variant="button" gutterBottom> { person.telephone } </Typography>
            </div>
        );
    }
}

export default PayeeFormatter;