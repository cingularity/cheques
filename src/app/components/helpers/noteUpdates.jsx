import React, { Component, Fragment } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import classNames from 'classnames';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';


const styles = theme => ({
    root: {
        //width: '100%',
        //maxHeight: 250,
        overflow: 'auto',
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing.unit * 2,
    },
    margin: {
        margin: theme.spacing.unit * 2,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '97%',
    },
});


@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class NoteUpdate extends Component {
    state = {
        edit: false,
        note: '',
        id: ''
    };
    handleChange = name => event => {
        this.setState({
            [name] : event.target.value
        });
    };

    handleSubmit = event => {
        event.preventDefault();
        const { store } = this.props;
        store.chequeStore.updateComment(this.state.id, this.state.note);
        store.uiStore.modalComment = false;
        this.setState({ edit: false});
    }

    handleEdit = (id, current_note) => event => {
        this.setState({ 
            edit: true, 
            id: id,
            note: current_note
        })
    };
    render() {
        const { id, classes, store } = this.props;
        const cheque = store.chequeStore.getCheque(id);
        if (this.state.edit) {
            return (
                <Paper className={classNames(classes.paper, classes.margin)}>
                    <form onSubmit={this.handleSubmit}>
                        <Typography>Editing cheque number {cheque._id}</Typography>
                        <TextField 
                            id="outlined-multiline-flexible"
                            label="Update note"
                            multiline
                            rowsMax="4"
                            value={this.state.note}
                            onChange={this.handleChange('note')}
                            className={classes.textField}
                            margin="normal"
                            helperText="Update the note for this bounced cheque."
                            variant="outlined"
                        />
                        <Button 
                            type="submit"
                        >
                            Update
                        </Button>
                    </form>
                </Paper>
            );
        } else {
            return (
                <Paper className={classNames(classes.paper, classes.margin)}>
                    <Typography variant="h6">Cheque No: {cheque._id}</Typography>
                    <Typography variant="subtitle1" className={classes.note}>{cheque.details.note}</Typography>
                    
                    <Button onClick={this.handleEdit(cheque._id, cheque.details.note)}>Edit</Button>
                </Paper>
            );
        }
    }
}