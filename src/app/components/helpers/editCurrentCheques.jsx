import React, { Component } from 'react';
import classNames from 'classnames';
import { observer, inject } from "mobx-react";
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import { 
    getBSToday, getADToday, getBSOneMonth, 
    getADOneMonth, checkBSDate, checkADDate,
    getADtoBS
} from '../../../services/nep_date';
import mapKeys from 'lodash/mapKeys';


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        maxHeight: 580,
        padding: theme.spacing.unit
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        flexBasis: 260,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 260,
        margin: 'dense'
    },
    button: {
        width: '60%',
        margin: theme.spacing.unit * 3,
    },
    grid: {
        //padding: theme.spacing.unit
    }
});


@withStyles(styles)
@inject('store')
@observer
export default class EditCurrentCheque extends Component {
    constructor(props) {
        super(props);
        this.state=({
            cheque_encash_date: '',
            cheque_received: ''
        });
    }

    componentDidMount() {
        const {id , store } = this.props;
        //get the current cheque
        const current_cheque = store.chequeStore.getCheque(id);
        // update the current cheque model in store.cheque
        store.cheque.setCurrentCheque(current_cheque);
        if (store.cheque.calendar==='BS') {
            this.setState({
                cheque_encash_date: getADtoBS(store.cheque.cheque_encash_date),
                cheque_received: getADtoBS(store.cheque.cheque_received)
            })
        } else {
            this.setState({
                cheque_encash_date: store.cheque.cheque_encash_date,
                cheque_received: store.cheque.cheque_received
            })
        }
        
        
    }

    componentWillUnmount() {
        this.props.store.cheque.resetCheque();
    }

    handleChange = field => event => {
        const { cheque } = this.props.store;
        cheque.updateVariable(field, event.target.value);
        //console.log(field, event.target.value)
    };
    handleCalendarBlur = field => event => {
        const { cheque, userStore } = this.props.store;
        if (userStore.calendar='BS') {
            const valid = checkBSDate(event.target.value)
            !valid ? cheque.updateVariable(field, '') : cheque.updateVariable(field, event.target.value);
            this.setState({
                [field]: event.target.value
            })
            return;
        } else {
            const valid = checkADDate(event.target.value)
            !valid ? cheque.updateVariable(field, '') : cheque.updateVariable(field, event.target.value);
            this.setState({
                [field]: event.target.value
            })
            return;
        }
    };

    handleCalendar = field => event => {
        const { cheque } = this.props.store;

    }

    handleSubmit = event => {
        event.preventDefault();
        const { cheque, chequeStore, userStore, uiStore, id } = this.props.store;
        //  set the calendar and convert the date 
        cheque.setCalendar(userStore.calendar);
        //chequeStore.addCheque(cheque.currentCheque);
        chequeStore.updateCheque(cheque.currentCheque)
        cheque.resetCheque();
        uiStore.modalEdit = false;
    }
    

    render() {
        const { classes, store, id } = this.props;
        const cheque = store.cheque;
        // const bsToday = getBSToday();
        // const bsInMonth = getBSOneMonth();
        return (
            <Paper>
                <Grid container className={classNames(classes.root, classes.margin)} spacing={8}>
                    <form onSubmit={this.handleSubmit}>
                        <Grid item xs md className={classNames(classes.margin, classes.grid)}>
                            <Typography
                                className={classNames(classes.margin, classes.textField)}
                            >
                                Cheque No: {cheque._id}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md className={classNames(classes.margin, classes.grid)}>
                            <TextField 
                                className={classNames(classes.margin, classes.textField)}
                                label="Received Date (YYYY-MM-DD)"
                                value={this.state.cheque_received}
                                onBlur={this.handleCalendarBlur('cheque_received')}
                                onChange={this.handleCalendar('cheque_received')}
                                InputProps={ 
                                    store.userStore.calendar === 'BS'
                                        ? {startAdornment: <InputAdornment position="start">BS</InputAdornment>,}
                                        : {startAdornment: <InputAdornment position="start">AD</InputAdornment>,}
                                } 
                                required 
                            />
                            <TextField 
                                className={classNames(classes.margin, classes.textField)}
                                label="Due Date (YYYY-MM-DD)"
                                value={this.state.cheque_encash_date}
                                onBlur={this.handleCalendarBlur('cheque_encash_date')}
                                onChange={this.handleCalendar('cheque_encash_date')}
                                InputProps={ store.userStore.calendar === 'BS'
                                    ? {startAdornment: <InputAdornment position="start">BS</InputAdornment>,}
                                    : {startAdornment: <InputAdornment position="start">AD</InputAdornment>,}
                                } 
                                required
                            />
                        </Grid>
                        <Grid item xs={12} md className={classNames(classes.margin, classes.grid)}>
                            <TextField
                                className={classNames(classes.margin, classes.textField)}
                                label="Amount"
                                value={cheque.amount}
                                onChange={this.handleChange('amount')}
                                required
                                InputProps={{startAdornment: <InputAdornment position="start">Rs</InputAdornment>,}}
                            />
                            <TextField
                                className={classNames(classes.margin, classes.textField)}
                                label="Note"
                                value={cheque.note}
                                onChange={this.handleChange('note')}
                                multiline
                            />
                        </Grid>
                        <Grid container spacing={0} justify="center" alignItems="flex-start" direction="column">
                            <Typography>Bank Details</Typography>
                            <Grid item xs md className={classNames(classes.margin, classes.grid)}>
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Bank"
                                    value={cheque.bank_name}
                                    onChange={this.handleChange('bank_name')}
                                    required
                                />
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Telephone"
                                    value={cheque.bank_telephone}
                                    onChange={this.handleChange('bank_telephone')}
                                />
                            </Grid>

                            <Typography>Person Details</Typography>
                            <Grid item xs md className={classNames(classes.margin, classes.grid)}>
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Name"
                                    value={cheque.person_name}
                                    onChange={this.handleChange('person_name')}
                                    required
                                />
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Company Name"
                                    value={cheque.person_company}
                                    onChange={this.handleChange('person_company')}
                                    required
                                />
                                <TextField
                                    className={classNames(classes.margin, classes.textField)}
                                    label="Telephone"
                                    value={cheque.person_telephone}
                                    onChange={this.handleChange('person_telephone')}
                                    //required
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            variant='outlined'
                            className={classes.button}
                        >
                            Update
                        </Button>
                    </form>
                </Grid>
            </Paper>
        );
    }
}