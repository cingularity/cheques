import React, { Component, Fragment } from 'react';
import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import classNames from 'classnames';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


const styles = theme => ({
    root: {
        //width: '100%',
        //maxHeight: 250,
        overflow: 'auto',
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing.unit * 2,
    },
    margin: {
        margin: theme.spacing.unit * 2,
    }
});

@withStyles(styles, { withTheme: true })
@inject('store')
@observer
export default class DeleteCheque extends Component {
    handleNoButtonPress = event => {
        const {uiStore} = this.props.store;
        uiStore.modalDelete=false;
    };

    handleYesButtonPress = _id => event => {
        const { store } = this.props;
        store.chequeStore.deleteOneCheque(_id)
        store.uiStore.modalDelete=false;
    }

    render() {
        const { id, classes, store } = this.props;
        const cheque = store.chequeStore.getCheque(id);
        console.log('deleteChe', cheque)
        return (
            <Paper className={classNames(classes.paper, classes.margin)}>
                <Typography variant="h6">Are you sure you want to delete this cheque</Typography>
                    <div>
                        <Typography variant="button">Cheque No: { cheque._id }</Typography>
                        <Typography variant="body1">Payee Name: { cheque.details.person.name }</Typography>
                        <Typography variant="body1">Payee's Company: { cheque.details.person.company === undefined ? '' : cheque.details.person.company }</Typography>
                        <Typography variant="body1">Amount: Rs. { cheque.details.amount }</Typography>
                    </div>
                <Button onClick={this.handleYesButtonPress(cheque._id)}>Yes</Button>
                <Button onClick={this.handleNoButtonPress}>No</Button>
            </Paper>
        );
    } 
}