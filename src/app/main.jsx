import React from 'react';
import { observer } from "mobx-react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

import Home from './components/home';
import AllCheques from './components/allCheques';
import ChequeEncashed from './components/encashed';




function TabContainer(props) {
    return (
        <Typography component="div">
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.secondary.light,
    },
    appbar: {
        backgroundColor: theme.palette.secondary.light,
        color: theme.palette.secondary.contrastText,
        height: 42
    },
    tabs: {
        height: 16,
    },
    tab: {
        fontSize: 12
    }
});


@withStyles(styles, { withTheme: true })
@observer
export default class Main extends React.Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.appbar}>
                <Tabs className={classes.tabs} value={value} onChange={this.handleChange}>
                    <Tab className={classes.tab} label="Current Status" />
                    <Tab className={classes.tab} label="Completed" />
                    <Tab className={classes.tab} label="Search" />
                </Tabs>
                </AppBar>
            {value === 0 && <TabContainer><Home /></TabContainer>}
            {value === 1 && <TabContainer><ChequeEncashed /></TabContainer>}
            {value === 2 && <TabContainer><AllCheques /></TabContainer>}
        </div>
        );
    }
}


