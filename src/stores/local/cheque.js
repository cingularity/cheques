import { observable, action } from 'mobx';
import { 
    getBStoAD, getBSToday, getBSOneMonth, getBS,
    getADToday, getADOneMonth, getAD
} from '../../services/nep_date';



export default class Cheque {
    @observable _id;
    @observable amount;
    @observable bank_name;
    @observable bank_telephone;
    @observable bounced;
    @observable cheque_encash_date;
    @observable cheque_received;
    @observable encashed;
    @observable note;
    @observable person_name;
    @observable person_company;
    @observable person_telephone;
    @observable sent_to_bank;
    @observable calendar;
    @observable notified;

    constructor () {
        this._id = '';
        this.amount = '';
        this.bank_name = '';
        this.bank_telephone = '';
        this.bounced = false;
        this.cheque_encash_date = '';
        this.cheque_received = '';
        this.encashed = false;
        this.note = '';
        this.person_name = '';
        this.person_company = '';
        this.person_telephone = '';
        this.sent_to_bank = false;
        this.calendar = '';
        this.notified = false;
    }

    @action.bound
    resetCheque() {
        this._id = '';
        this.amount = '';
        this.bank_name = '';
        this.bank_telephone = '';
        this.bounced = false;
        this.cheque_encash_date = '';
        this.cheque_received = '';
        this.encashed = false;
        this.note = '';
        this.person_name = '';
        this.person_company = '';
        this.person_telephone = '';
        this.sent_to_bank = false;
        this.calendar = '';
        this.notified = false;
    }

    @action.bound
    setCalendar(cal) {
        this.calendar = cal;
        if (cal === 'BS') {
            try {
                this.cheque_received = getBStoAD(this.cheque_received).toISOString();
                this.cheque_encash_date = getBStoAD(this.cheque_encash_date).toISOString();
                //console.log('received', this.cheque_received, 'encahc', this.cheque_encash_date)
            } catch (e) {
                console.log('BS calendar error: ', e);
                this.cheque_received = '';
                this.cheque_encash_date = '';
            }
            
        } else {
            try {
                this.cheque_received = new Date(this.cheque_received).toISOString();
                this.cheque_encash_date = new Date(this.cheque_encash_date).toISOString();
            } catch (e) {
                console.log('BS calendar error: ', e);
                this.cheque_received = '';
                this.cheque_encash_date = '';
            }
        }
    }

    @action.bound
    updateVariable(field, value) {
        this[field] = value;
    }


    get currentCheque() {
        return {
            _id: this._id, 
            details: {
                amount: this.amount,
                bank: {
                    name: this.bank_name,
                    telephone: this.bank_telephone,
                },
                bounced: this.bounced,
                cheque_encash_date: this.cheque_encash_date,
                cheque_received: this.cheque_received,
                encashed: this.encashed,
                note: this.note,
                person: {
                    name: this.person_name,
                    company: this.person_company,
                    telephone: this.person_telephone
                },
                sent_to_bank: this.sent_to_bank,
                calendar: this.calendar,
                notified: this.notified
            }
        }
    }

    @action.bound
    setCurrentCheque(cheque) {
        this._id = cheque._id;
        this.amount = cheque.details.amount;
        this.bank_name = cheque.details.bank.name;
        this.bank_telephone = cheque.details.bank.telephone;
        this.bounced = cheque.details.bounced;
        this.cheque_encash_date = cheque.details.cheque_encash_date;
        this.cheque_received = cheque.details.cheque_received;
        this.encashed = cheque.details.encashed;
        this.note = cheque.details.note;
        this.person_name = cheque.details.person.name;
        this.person_company = cheque.details.person.company;
        this.person_telephone = cheque.details.person.telephone;
        this.sent_to_bank = cheque.details.sent_to_bank;
        this.calendar = cheque.details.calendar;
        this.notified = cheque.details.notified;
    }
}