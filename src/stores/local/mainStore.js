import { observable } from 'mobx';
import ChequeStore from './chequeStore';
import UserStore from './userStore';
import UIStore from './uiStore';
import Cheque from './cheque';
import db from '../persistent/db';


class MainStore {
    @observable chequeStore;
    @observable userStore;
    @observable cheque = new Cheque();
    @observable uiStore = new UIStore();


    getCheques = async () => {
        const documents = await db.cheques.find({});
        return documents
    };

    // cheques= async() =>{
    //     //console.log(await this.getCheques())
    //     return await this.getCheques()
    // }

    constructor() {
        // get the persistant database and update the chequeStore
        this.chequeStore = new ChequeStore();
        this.userStore = new UserStore();
        
        //const value = await userStore();
        this.getCheques().then(content => this.chequeStore.cheques=content);
        //console.log(this.cheques())
    }

}

var store = window.store = new MainStore();
export default store;