import { observable, action } from 'mobx';

export default class UIStore {
    @observable modal_comment=false;
    @observable modal_edit=false;
    @observable modal_delete=false;
    @observable modal_add_cheque=false;
    @observable modal_encash=false;

    set modalComment(value) {
        this.modal_comment=value;
    };

    get modalComment() {
        return this.modal_comment;
    }
    
    set modalEdit(value) {
        this.modal_edit=value;
    };

    get modalEdit() {
        return this.modal_edit;
    }

    set modalDelete(value) {
        this.modal_delete=value;
    };

    get modalDelete() {
        return this.modal_delete;
    }

    set modalAddCheque(value) {
        this.modal_add_cheque=value;
    };

    get modalAddCheque() {
        return this.modal_add_cheque;
    }
    set modalEncash(value) {
        this.modal_encash=value;
    };

    get modalEncash() {
        return this.modal_encash;
    }
}