import { observable } from 'mobx';

export default class UserStore {
    @observable calendar = 'BS' // options are 'AD' and 'BS'
    @observable username = ''


    set calendar(option) {
        this.calendar = option;
    }


    get calendar() {
        return this.calendar;
    }
}