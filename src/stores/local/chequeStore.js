import { observable, computed, action, runInAction, toJS } from 'mobx';
import findIndex from 'lodash/findIndex';
import db from '../persistent/db';
import searchFunc from '../../services/search';

// stores all the cheques
export default class ChequeStore {
    @observable cheques = [];
    @observable current_id = null;
    @observable search = '';
    @observable searchResults=[];
    @observable encashed_in='';

    updateDB = async (_id, changes) => {
        const response = await db.cheques.update(
            {_id: _id.toString()},
            { $set: changes}
        );
        return response;
    }

    removeFromDB = async(_id) => {
        const response = await db.cheques.remove(
            {_id: _id.toString()},
            {}
        );
        return response;
    }

    set Search(value) {
        this.search = value;
    }

    get Search() {
        return this.search;
    }

    set CurrentID(id) {
        this.current_id = id;
    }

    get CurrentID() {
        return this.current_id;
    }

    // About Notification
    @action 
    hideNotification(_id) {
        // update local mobx and only hide for this session
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        this.cheques[index].details.notified = true;
    }


    // notification aknowledged, will show no more
    @action
    removeNotification(_id) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        this.cheques[index].details.notified = true;
        const changes = {
            'details.notified' : true,
        };
        // update persistant database
        this.updateDB(_id, changes).then(value => console.log(value));
    }
    // end Notification

    @action
    startSearch(search_string) {
        this.searchResults = searchFunc(toJS(this.all), search_string)
    }

    @computed
    get searched() {
        return this.searchResults.length === 0 || this.search ==='' ? toJS(this.all): this.searchResults;
    }

    @computed
    get all() {
        return this.cheques;
    }

    // all the current Cheques, that are new
    @computed
    get current() {
        return this.cheques.filter(
            cheque => (
                cheque.details.bounced === false &&
                cheque.details.sent_to_bank === false &&
                cheque.details.bounced === false &&
                cheque.details.encashed === false
            )
        );
    }


    set markAsBack(_id) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        
        runInAction(() => {
            // update the mobx array
            this.cheques[index].details.encashed=false;
            this.cheques[index].details.sent_to_bank=false;
            this.cheques[index].details.bounced=false;
            const changes = {
                'details.sent_to_bank' : false,
                'details.encashed': false,
                'details.bounced': false,
            };
            // update persistant database
            this.updateDB(_id, changes).then(value => console.log(value));
        })
    }

    set markAsSentToBank(_id) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        
        runInAction(() => {
            // update the mobx array
            this.cheques[index].details.encashed=false;
            this.cheques[index].details.sent_to_bank=true;
            this.cheques[index].details.bounced=false;
            const changes = {
                'details.sent_to_bank' : true,
                'details.encashed': false,
                'details.bounced': false,
            };
            // update persistant database
            this.updateDB(_id, changes).then(value => console.log(value));
        })
    }

    // end current cheques

    // all the cheques sent to bank, it can be either sent to encashed or to bounced
    @computed
    get sentToBank() {
        return this.cheques.filter(
            cheque => (
                cheque.details.sent_to_bank === true
            )
        );
    }

    set markAsEncashed(_id) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        
        runInAction(() => {
            // update the mobx array
            this.cheques[index].details.encashed=true;
            this.cheques[index].details.sent_to_bank=false;
            this.cheques[index].details.bounced=false;
            this.cheques[index].details.encahed_in = this.encashed_in;
            // update persistant database
            const changes = {
                'details.sent_to_bank' : false, 
                'details.encashed': true,
                'details.bounced': false,
                'details.encashed_in': this.encashed_in
            }
            this.updateDB(_id, changes).then(value => console.log(value))
        });
        this.encashed_in = ''
    }

    set markAsBounced(_id) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        
        runInAction(() => {
            // update the mobx array
            this.cheques[index].details.encashed=false;
            this.cheques[index].details.sent_to_bank=false;
            this.cheques[index].details.bounced=true;
            // update persistant database
            const changes = {
                'details.sent_to_bank' : false, 
                'details.encashed': false,
                'details.bounced': true,
            }
            this.updateDB(_id, changes).then(value => console.log(value))
        })
    }

    // end Sent to bank

    
    // all the bounced cheques are shown here, it can only be deleted after this
    @computed
    get bounced() {
        return this.cheques.filter(
            cheque => cheque.details.bounced === true
        );
    }

    // end of bounced

    // all the encashed cheques are shown here
    @computed
    get encashed() {
        return this.cheques.filter(
            cheque => cheque.details.encashed === true
        );
    };

    // end of all encashed

    insertCheque = async (cheque) => {
        const response = await db.cheques.insert(cheque);
        return response;
    }

    // get single cheque for update
    @action
    getCheque(id) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === id);
        console.log('returning index store', index)
        return this.cheques[index];
    }

    set onEncash(choice) {
        // on encash choose between cash and bank
        this.encashed_in = choice 
    }

    @action
    updateCheque(changed_cheque) {
        const _id = parseInt(changed_cheque._id)
        console.log('_id: ',_id)
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        this.cheques.splice(index, 1, changed_cheque);
        const changes = {
            'details': changed_cheque.details,
        }
        this.updateDB(_id, changes).then(value => console.log(value));
    }
    
    @action
    updateComment(_id, note) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id);
        this.cheques[index].details.note = note;
        const changes = {
            'details.note' : note, 
        }
        this.updateDB(_id, changes).then(value => console.log(value))
    }

    // this is check if a cheque exists and either add or update.
    async addCheque(cheque_to_add) {
        // first check if the cheque exists
        const index = findIndex(this.cheques, (cheque) => cheque._id === cheque_to_add._id)
        // no index found
        if (index === -1) {
            this.cheques.push(cheque_to_add)
            // add to persistent database
            const answer = await this.insertCheque(cheque_to_add);
            //console.log(answer)
        } else {
            // if the index is there
            this.cheques.splice(index, 1, cheque_to_add)
            //update in persistent database too
            this.updateDB(cheque_to_add._id, cheque_to_add)
        }
    }

    @action
    deleteOneCheque(_id) {
        const index = findIndex(this.cheques, (cheque) => cheque._id === _id)
        if (index === -1) {
            return;
        }
        runInAction(() => {
            // delete from mobx
            this.cheques.splice(index, 1);
            // delete from db
            this.removeFromDB(_id).then(result => console.log(result))
        })
    }

    // delete a cheque
    @action
    deleteCheque(cheque_to_delete) {
        // first check if the cheque exists
        const index = findIndex(this.cheques, (cheque) => cheque._id === cheque_to_delete._id)
        if (index === -1) {
            return;
        } else {
            this.cheques.splice(index, 1);
        }
    }
}