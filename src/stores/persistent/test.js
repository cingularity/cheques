//import db from './db';

const db = require('./db');

const addCheque = async (cheque) => {
    const response = await db.cheques.insert(cheque)
    return response;
}

var cheque1 = {
    "_id": 2896,
    "details": {
        "amount": "503.42",
        "bank": {
            "name": "PNBank",
            "telephone": "00-67583201"
        },
        "bounced": false,
        "cheque_encash_date": "2019-05-25T20:41:43.698Z",
        "cheque_received": "2019-04-13T20:41:43.698Z",
        "encashed": false,
        "note": "Interdum et malesuada fames",
        "person": {
            "company": "Anayatech dairy",
            "name": "Anaya Shah",
            "telephone": "00-92382121"
        },
        "sent_to_bank": false
    }
}

addCheque(cheque1);