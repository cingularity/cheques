const Datastore = require('nedb-promises');

const dbFactory = fileName => Datastore.create({
    filename: `./database/${fileName}`,
    timestampData: true,
    autload: true
});



const db = {
    cheques: dbFactory('cheques.json'),
    users: dbFactory('user.json')
};


export default db;
